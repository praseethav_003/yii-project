<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;


use Yii;   
use yii\console\Controller;
use app\models\User;

/**
 * This command will insert user details
 *
 */
class UserController extends Controller
{
    /**
     * This command will insert user details from json file.
     */
    public function actionIndex()
    {
		$connection = \Yii::$app->db;
		$json	=	file_get_contents('users.json');
		$data 	=	json_decode($json);
		if(!empty($data)){
			foreach($data as $obj) {
				$model = User::find($obj->id);
				if(empty($model)){
					$connection->createCommand()->insert('user',
					[
						'id' 			=> $obj->id,
						'first_name' 	=> $obj->first_name,
						'last_name' 	=> $obj->last_name,
						'email' 		=> $obj->email,
						'personal_code' => $obj->personal_code,
						'phone' 		=> $obj->phone,
						'active' 		=> $obj->active,
						'dead' 			=> $obj->dead,
						'lang' 			=> $obj->lang
					])
					->execute();
				}
			}
			echo 'User details inserted';
		}else{
			echo 'No user data found';
		}
    }
}

<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;


use Yii;   
use yii\console\Controller;
use app\models\Loan;

/**
 * This command will insert loan details
 *
 */
class LoanController extends Controller
{
    /**
     * This command will insert loan details from json file.
     */
    public function actionIndex()
    {
		$connection = \Yii::$app->db;
		$json		= file_get_contents('loans.json');
		$data 		= json_decode($json);
		if(!empty($data)){
			foreach($data as $obj) {
				$start_date	=	date('Y-m-d',$obj->start_date);
				$model = Loan::find()
				->where(['user_id' => $obj->user_id, 'start_date' => $start_date])
				->one();
				if(empty($model)){
					$connection->createCommand()->insert('loan',
					[
						'user_id' 		=> $obj->user_id,
						'amount' 		=> $obj->amount,
						'interest' 		=> $obj->interest,
						'duration' 		=> $obj->duration,
						'start_date' 	=> date('Y-m-d',$obj->start_date),
						'end_date' 		=> date('Y-m-d',$obj->end_date),
						'campaign' 		=> $obj->campaign,
						'status' 		=> $obj->status
					])
					->execute();
				}
			}
			echo 'Loan details are inserted';
		}else{
			echo 'No loan data found';
		}
    }
}
